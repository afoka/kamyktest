﻿<?php

class A
{
	private function operacja1()
	{
		echo "operacja1 wywołana";
	}
	protected function operacja2()
	{
		echo "operacja2 wywołana";
	}
	public function operacja3()
	{
		echo "operacja3 wywołana";
	}
	public $atrybut;
	function operacja4($param)
	{
		$this->$atrybut = $param;
		echo $this->$atrybut;
	}
}

class B extends A
{
	function __construct()
	{
		//$this->operacja1();
		$this->operacja2();
		$this->operacja3();
		//$this->operacja4();
	}
}
$b = new B;
$b->operacja3();
//$param ="parameter"
$b->operacja4("trawa");
?>